import React from "react";
import "../../../todoapptemplate/src/css";
import "../todoapptemplate/src/assets/fonts";
import ReactDOM from "react-dom";
import { Provider } from "mobx-react";
import RootStore from "../src/app/stores/RootStore";
import App from "../src/App";

ReactDOM.render(
	<Provider {...new RootStore()}>
		<App />
	</Provider>,
	document.getElementById("root")
);
