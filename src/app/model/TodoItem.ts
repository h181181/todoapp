import { observable, action, computed } from "mobx";

class TodoItem {
	readonly id?: string;
	readonly ref?: string;
	@observable title: string;
	@observable category: string;
	@observable startDate: Date;
	@observable endDate: Date;

	constructor(
		title: string,
		category: string,
		startDate: Date,
		endDate: Date,
		id?: string,
		ref?: string
	) {
		(this.title = title),
			(this.category = category),
			(this.startDate = startDate),
			(this.endDate = endDate),
			(this.id = id),
			(this.ref = ref);
	}

	ownerToJson() {
		return {
			id: this.id,
			ref: this.ref,
			title: this.title,
			category: this.category,
			startDate: this.startDate,
			endDate: this.endDate
		};
	}

	@action.bound
	setTitle(title: string): void {
		this.title = title;
	}

	@action.bound
	changeTitle(e): void {
		this.title = e.target.value;
	}

	@action.bound
	setCategory(category: string): void {
		this.category = category;
	}

	@action.bound
	changeCategory(e): void {
		this.category = e.target.value;
	}

	@action.bound
	setStartDate(startDate: Date): void {
		this.startDate = startDate;
	}

	@action.bound
	changeStartDate(e): void {
		this.startDate = e.target.value;
	}

	@action.bound
	setEndDate(endDate: Date): void {
		this.endDate = endDate;
	}

	@action.bound
	changeEndDate(e): void {
		this.endDate = e.target.value;
	}
}
export default TodoItem;
