import { observable, action, computed, autorun } from "mobx";

class TodoStore {
	@observable todoitems;
	@observable categories: string[];
}
