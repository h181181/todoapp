import ViewStore from "./ViewStore";
import TodoItem from "../model/TodoItem";
import Store from "./Store";

class RootStore {
	todoStore: Store<TodoItem>;
	viewStore: ViewStore;

	constructor() {
		this.todoStore = new Store<TodoItem>();
		this.viewStore = new ViewStore();
	}
}

export default RootStore;
