import { observable, action } from "mobx";

class ViewStore {
	@observable currentPage: string;
	@observable currentFormOpen: boolean;
	@observable targetFormOpen: boolean;

	constructor(currentPage?: string) {
		this.currentPage = this.currentPage;
		this.currentFormOpen = false;
		this.targetFormOpen = false;
	}

	@action.bound
	setCurrentPage(page: string): void {
		this.currentPage = page;
	}

	@action.bound
	setCurrentFormOpen(b: boolean): void {
		this.currentFormOpen = b;
	}

	@action.bound
	setTargetFormOpen(b: boolean): void {
		this.targetFormOpen = b;
	}
}

export default ViewStore;
