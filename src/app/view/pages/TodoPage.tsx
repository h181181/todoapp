import React from "react";
import { inject, observer } from "mobx-react";

@inject("todoStore", "todoView")
@observer
class TodoPage extends React.Component<any, any> {
	render() {
		let {
			rootView: {
				locationView: { setCurrentFormOpen }
			}
		} = this.props;
		let {
			rootView: { theme }
		} = this.props;
		let {
			authStore: { loggedInUser }
		} = this.props;

		return (
			<div style={{ margin: "20px" }}>
				<h3
					style={{
						color: theme.currentTheme.textMain,
						margin: "15px 10px 5px 10px"
					}}
				>
					Todo List
				</h3>
			</div>
		);
	}
}

export default TodoPage;
