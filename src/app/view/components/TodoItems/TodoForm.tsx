import React from "react";
import { observable, action, computed } from "mobx";
import { inject, observer } from "mobx-react";


@inject("viewStore", "todoStore")
@observer
class TodoForm extends React.Component<any, any> {

	handleFormClose = () => {
		let {
			rootView: {
				coachView: { setCurrentFormOpen, setTargetFormOpen }
			}
		} = this.props;
		let {
			coachStore: { target, current, setTarget, setCurrent }
		} = this.props;
		if (target) {
			setTargetFormOpen(false);
			setTarget(null);
		}
		if (target && current) {
			setCurrentFormOpen(false);
			setCurrent(null);
		} else {
			setCurrentFormOpen(false);
			setCurrent(null);
		}
	};

	handleSelectChangePerson = e => {
		let targetValue = e.target.value;
		var person = this.props.personStore.list.find(person => person.firstName === targetValue);
		this.props.coach.setPerson(person);
	};

	submitCurrentCoach = () => {
		// databaseAccessor.create(
		// 	"coaches",
		// 	this.props.coachStore.current.coachToJson(),
		// 	this.props.authStore.loggedInUser.token
		// );
		// this.props.coachStore.add(this.props.coachStore.current);
		// this.props.coachStore.scrollToBottom();
		// //close modal
		// this.props.rootView.coachView.setCurrentFormOpen(false);
	};

	render() {
		let { todo } = this.props;
		let target = this.props.coachStore.target;
		let todos = this.props.todoStore.list;
		return (
            
        <div>
            <div className="dialog is-shown dialog--frameless">
    <div className="dialog__overlay"></div>
    <div className="dialog__container">
        <div className="dialog__background">
            <header className="dialog__title">
                Frameless dialog
                <a className="dialog__close" href="#" title="Close dialog"></a>
            </header>
            <section className="dialog__content">
                <div className="list">
                    <ul className="list__list">
                        <li className="list__item">
                            <div className="item__content">
                                <div className="content__column content__column--primary xs-100 sm-60 md-60 lg-60 xl-60">
                                    <h4 className="item__title">
                                        List item title
                                    </h4>
                                    <div className="item__description">
                                        Congue eu consequat ac felis donec et odio, congue eu consequat ac felis donec et odio.
                                    </div>
                                </div>
                                <div className="content__column xs-50 sm-25 md-25 lg-10 xl-10">
                                    <div className="item__meta">
                                        Additional information
                                    </div>
                                </div>
                                <div className="content__column has-alignment-right xs-50 sm-5 md-5 lg-20 xl-20">
                                    <span className="badge has-bgColor-stateBlue100 has-color-snow">
                                        3
                                    </span>
                                    <i aria-hidden="true" className="iconMdsp information black has-color-stateBlue100"></i>
                                </div>
                            </div>
                        </li>
                        <li className="list__item">
                            <div className="item__content">
                                <div className="content__column content__column--primary xs-100 sm-60 md-60 lg-60 xl-60">
                                    <h4 className="item__title">
                                        Next list item title
                                    </h4>
                                </div>
                                <div className="content__column xs-50 sm-25 md-25 lg-10 xl-10">
                                    <div className="item__meta">
                                        Metainfo
                                    </div>
                                </div>
                                <div className="content__column has-alignment-right xs-50 sm-5 md-5 lg-20 xl-20">
                                    <span className="badge has-bgColor-functionalYellow has-color-snow">
                                        45
                                    </span>
                                    <i aria-hidden="true" className="iconMdsp exclamationDiamond black has-color-functionalYellow"></i>
                                </div>
                            </div>
                        </li>
                        <li className="list__item">
                            <div className="item__content">
                                <div className="content__column content__column--primary xs-100 sm-60 md-60 lg-60 xl-60">
                                    <h4 className="item__title">
                                        Another list item
                                    </h4>
                                    <div className="item__description">
                                        No sea takimata sanctus.
                                    </div>
                                </div>
                                <div className="content__column xs-50 sm-25 md-25 lg-10 xl-10">
                                    <div className="item__meta">
                                        Meta information
                                    </div>
                                </div>
                                <div className="content__column has-alignment-right xs-50 sm-5 md-5 lg-20 xl-20">
                                    <i aria-hidden="true" className="iconMdsp ok black has-color-functionalGreen"></i>
                                </div>
                            </div>
                        </li>
                        <li className="list__item">
                            <div className="item__content">
                                <div className="content__column content__column--primary xs-100 sm-60 md-60 lg-60 xl-60">
                                    <h4 className="item__title">
                                        List item
                                    </h4>
                                    <div className="item__description">
                                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr.
                                    </div>
                                </div>
                                <div className="content__column xs-50 sm-25 md-25 lg-10 xl-10">
                                </div>
                                <div className="content__column has-alignment-right xs-50 sm-5 md-5 lg-20 xl-20">
                                    <span className="badge has-bgColor-validationRed has-color-snow">
                                        258
                                    </span>
                                    <i aria-hidden="true" className="iconMdsp attention black has-color-validationRed"></i>
                                </div>
                            </div>
                        </li>
                        <li className="list__item">
                            <div className="item__content">
                                <div className="content__column content__column--primary xs-100 sm-60 md-60 lg-60 xl-60">
                                    <h4 className="item__title">
                                        List item title
                                    </h4>
                                    <div className="item__description">
                                        Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat sed diam voluptua. At vero eos etaccusam et justo duo dolores.
                                    </div>
                                </div>
                                <div className="content__column xs-50 sm-25 md-25 lg-10 xl-10">
                                    <div className="item__meta">
                                        Information
                                    </div>
                                </div>
                                <div className="content__column has-alignment-right xs-50 sm-5 md-5 lg-20 xl-20">
                                    <i aria-hidden="true" className="iconMdsp ok black has-color-functionalGreen"></i>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
        </div>
    </div>
</div>
</div>
        );
	}
}

export default TodoForm;
