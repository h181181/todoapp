import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { inject, observer } from "mobx-react";
import TodoPage from "./app/view/pages/TodoPage";

@inject("todoView")
@observer
class App extends Component<any, any> {
	render() {
		let {
			todoView: { currentPage }
		} = this.props;

		return (
			<div>
				{/* {currentPage !== "loginPage" && <Appbar />} */}
				{currentPage === "todoPage" && <TodoPage />}
				{/* {currentPage === "ganttPage" && <PlayerPage />} */}
			</div>
		);
	}
}

export default App;
